/*Définition des vecteurs 2D et fonctions utilitaires*/


typedef struct Vector2D{
    int x;
    int y;
}Vector2D;


Vector2D newVector2D(int x, int y);


Vector2D add(Vector2D v1, Vector2D v2);
Vector2D sub(Vector2D v1,Vector2D v2);

void set(Vector2D * pvector2D, int x , int y);

Vector2D scaledVersion(Vector2D vector2D, int scalar);
Vector2D map(Vector2D vector2D, int (*f)(int));