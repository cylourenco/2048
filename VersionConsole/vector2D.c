#include "vector2D.h"


Vector2D newVector2D(int x, int y)
{
    //Crée un nouveau Vector2D
    Vector2D vector2D = {x,y};
    
    return vector2D;
}


Vector2D add(Vector2D v1, Vector2D v2)
{
    //Retourne v1 + v2    
    Vector2D vector2D;
    vector2D.x = v1.x + v2.x;
    vector2D.y = v1.y + v2.y;

    return vector2D;
}

void set(Vector2D * pvector2D, int x , int y)
{
    //Mettre le vecteur à une valeur particulière
    pvector2D->x = x;
    pvector2D->y = y;
}

Vector2D scaledVersion(Vector2D vector2D, int scalar)
{
    //Retourne scalar * vector2D
    Vector2D scaledVector2D;
    scaledVector2D.x = scalar * vector2D.x;
    scaledVector2D.y = scalar * vector2D.y;

    return scaledVector2D;
}



Vector2D map(Vector2D vector2D, int (*f)(int))
{
    //Retourne un vecteur dont les coordonnées sont celles de vector2d auxquelles  on a appliqué la fonction f 
    Vector2D mappedVector2D;
    mappedVector2D.x = f(vector2D.x);
    mappedVector2D.y = f(vector2D.y);

    return mappedVector2D;
}

Vector2D sub(Vector2D v1,Vector2D v2)
{
    //Retourne v1 - v2
    return add(v1,scaledVersion(v2,-1));
}


