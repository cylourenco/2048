#include "grid.h"


void pop(grid * g, int a)
{
    /*
        si a = 1 c'est à dire au début de la partie:
            Met un 2 sur une case aléatoire du niveau de grid
        si a = 0 c'est à dire au cours de la partie:
            Met un 2 ou un 4 (uniformément choisi) sur une case aléatoire du niveau de grid
    */
    srand(time(NULL));
    
    if (g->nb_case_vide != 0)
    {
        
        int b;
        if (a == 1){
            b = 2;
        }
        else{
            b = (rand() % 2 == 1) ? 2 : 4;
        }

        int i,j;
        i = rand() % g->taille;
        j =  rand() % g->taille;
        while (g->level[i][j] != 0)
        {
            i = rand() % g->taille;
            j =  rand() % g->taille;
        } 

        g->level[i][j] = b;

        g->nb_case_vide -= 1;  
    }
    
}

int deplacement(grid * g, char c)
{  

    int change = 0;
    int fusion_flag = 0;

    Vector2D vec = {0,0};  //vecteur déplacement
    Vector2D dir;  //vecteur de direction du déplacemnent

    if (c == 'g')
    {
        set(&dir,-1,0);

        for (int i = 0; i < g->taille; i += 1) //Sur chaque ligne
        {
            //Réinitialiser le vecteur de déplacement et le marqueur de fusion
            set(&vec,0,0);
            fusion_flag = 0;

            for (int j=0; j < g->taille; j += -dir.x)  //Sur chaque colonnne
            {
                //Si on rencontre un 0, "incrémenter" le vecteur et préciser qu'aucune fusion n'a été faite
                if (g->level[i][j] == 0)  
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else //Sinon
                {

                    /*S'il doit y avoir un déplacement, préciser qu'un changement a eu lieu*/
                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  


                    Vector2D pos = {j,i};  //Le vecteur position de la case actuelle
                    Vector2D newpos = add(pos,vec); // calculer le vecteur position futur en ajoutant le vecteur déplacement 
                                                    // à la position actuelle 
                    Vector2D checkpos = add(newpos,dir); // la position juste après la position future

                    /*Si la case suivant celle où devrait se finir le déplacement à la même valeur que la 
                    case déplacée, mettre cette case au double et la case qui devrait être déplacée à 0 si aucune fusion
                    ne s'est déroulée à l'itération précédente. 
                    Sinon, déplacer la case à l'emplacement prévu et préciser qu'il n'y a eu aucune fusion
                    */
                    if (newpos.x > 0 && g->level[checkpos.y][checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                        //fusion 
                        g->level[checkpos.y][checkpos.x] *= 2;  // mise à jour du score
                        g->level[i][j] = 0;  
                        g->score += g->level[checkpos.y][checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;
                        fusion_flag = 1;
                    } 
                    else 
                    {
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);  //déplacement
                    }    

                    
                } 

            } 
        }   
    }
    else if (c == 'd')
    {
       set(&dir,1,0);

       for (int i = 0; i < g->taille; i += 1)
        {
            fusion_flag = 0;
            set(&vec,0,0);

            for (int j = g->taille - 1; j >= 0; j += -dir.x)
            {
                if (g->level[i][j] == 0)
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else
                {

                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  
                    Vector2D pos = {j,i};
                    Vector2D newpos = add(pos,vec);
                    Vector2D checkpos = add(newpos,dir);

                    if (newpos.x < g->taille - 1 && g->level[checkpos.y][checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                          
                        g->level[checkpos.y][checkpos.x] *= 2;
                        g->level[i][j] = 0; 
                        g->score += g->level[checkpos.y][checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;
                        fusion_flag = 1;
                    } 
                    else
                    {
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);
                    }  

                    
                } 

            }
        }   
    }  
    else if (c == 'b')
    {
       set(&dir,0,1);

       for (int j = 0; j < g->taille; j += 1)
        {
            set(&vec,0,0);
            fusion_flag = 0;

            for (int i=g->taille - 1; i >= 0; i += -dir.y)
            {
                if (g->level[i][j] ==   0)
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else
                {
                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  
                    Vector2D pos = {j,i};
                    Vector2D newpos = add(pos,vec);
                    Vector2D checkpos = add(newpos,dir);

                    if (newpos.y < g->taille - 1 && g->level[checkpos.y][checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                        //fusion
                        
                        g->level[checkpos.y][checkpos.x] *= 2;
                        g->level[i][j] = 0; 
                        g->score += g->level[checkpos.y][checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;
                        fusion_flag = 1;
                    
                    } 
                    else 
                    {
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);
                    }             
                } 

            }
        }
    }
    else if (c == 'h')
    {
        set(&dir,0,-1);

        for (int j = 0; j < g->taille; j += 1)
        {
            set(&vec,0,0);
            fusion_flag = 0;

            for (int i=0; i < g->taille; i += -dir.y)
            {
                if (g->level[i][j] ==   0)
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else
                {

                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  
                    Vector2D pos = {j,i};
                    Vector2D newpos = add(pos,vec);
                    Vector2D checkpos = add(newpos,dir);

                    if (newpos.y > 0 && g->level[checkpos.y][checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                        //fusion
                        
                        g->level[checkpos.y][checkpos.x] *= 2;
                        g->level[i][j] = 0; 
                        g->score += g->level[checkpos.y][checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;
                        fusion_flag = 1;
                    } 
                    else 
                    {
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);
                    }  

                    
                } 

            }
        }
    }  

    
    return change;

}


void reload(grid * g,char * filename)
{

    /*
        Chargement d'une grille sauvegardée dans filename
    */
    FILE * fichier = NULL;

    if ((fichier = fopen(filename,"r+")) != NULL)
    {
        fscanf(fichier,"%d\n",&g->taille);
        g->level = niveau(g->taille);

        for (int i=0; i < g->taille * g->taille;i++)
        {
            if (i % g->taille != g->taille - 1)
            {
                fscanf(fichier,"%d ",&g->level[i/g->taille][i%g->taille]);
            }
            else
            {
                fscanf(fichier,"%d\n",&g->level[i/g->taille][i%g->taille]);
            }
        }        

        fscanf(fichier,"%d\n",&g->score);
        fscanf(fichier,"%d\n",&g->nb_case_vide);

    }


}

grid initGrid()
{
    /*
        Initialisation de la grille
    */
    grid gr;
    printf("Choisis la taille de la grille entre 2 et 8: ");
    scanf("%d", &gr.taille);
    printf("\n");

    gr.level = niveau(gr.taille);
    gr.nb_case_vide = gr.taille * gr.taille ;

    pop(&gr,1);
    pop(&gr,1);
    gr.score = 0;

    return gr;
}

void save(grid gr,char * filename)
{
    /*
        Sauvegarde de la partie actuelle dans le fichier filename
    */
    FILE *fichier;
 
    fichier = fopen (filename, "w+");
    
    if (fichier != NULL)
    {
        fprintf(fichier,"%d\n",gr.taille);
        for(int i=0; i<gr.taille ; i++)
        {
            for(int j=0; j<gr.taille ; j++)
            { 
                fprintf (fichier, "%d ", gr.level[i][j]);
            }
            fprintf (fichier, "\n");
        }
        fprintf(fichier, "%d\n", gr.score);
        fprintf(fichier, "%d\n",gr.nb_case_vide);
        fclose (fichier);
    }

}

void affichePartie(grid gr)
{
    /*
        Affichage de l'état de la partie en cours
    */
    print_tab2D(gr.level,gr.taille);
    printf("Score: %d\n", gr.score);
    msleep(500);
}
