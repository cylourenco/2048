#include "grid.h"


int main(){
    int choix; 
    grid gr; 

MainScreen:

    printf("\n\n2048 de Cynthia Lourenço et Boris Ouya\n\n");
    printf("Utilisez g, d, h, b pour bouger les tuiles. \nLes tuiles avec le même numéro fusionnent en une seule lorsqu'elles se touchent. \nAdditionnez-les pour atteindre 2048 ! \n\n");


    printf("MENU: \n 1.Nouvelle Partie\n 2.Recharger la dernière partie sauvegardée\n 3.Quitter\nVotre choix:");
    scanf(" %d", &choix);

    switch (choix)
    {
        case 1:
            gr = initGrid();
            break;
        case 2:
            reload(&gr,"sauvegarde.txt");
            break;
        case 3:
            goto Exit;
            break;
        default:
            printf("Option invalide\n");
            goto Exit;
            break;
    }



    char c;


    while (1)
    {
        
        affichePartie(gr);
        printf("g: gauche, d: droite, h: haut, b: bas, s: sauvegarder et quitter, q: quitter sans sauvegarder\n");
        
        printf("Entrez votre choix:");
        rewind(stdin);
        scanf(" %c",&c);
        getchar();

        if (c == 'q')     // Quitter le jeu sans sauvegarder
        {
            goto Liberate;
        }
        else if (c == 's')      // Sauvegarde la partie dans un fichier texte et quitter le jeu
        {
            save(gr,"sauvegarde.txt");
            goto Liberate;
        }
        else if (c == 'g' || c =='d' || c =='h' || c == 'b')   // Le joueur continue de jouer en déplacant les cases
        {
            if (deplacement(&gr,c) == 1)
            {
                pop(&gr,0);
            }
            
        }
        else
        {
            printf("Entrée incorrecte\n");
        }

        
        int death_flag = 0;
        if (gr.nb_case_vide == 0){    // Si il n'y a plus de case de case vide et si il y a plus aucune combinaison possible alors le joueur a perdu, le jeu s'arrête et on revient au menu
        
            death_flag = 1;
            for(int i=0; i<gr.taille ; i++)
            {
                for(int j=0; j<gr.taille ; j++)
                { 
                    if ((j < gr.taille - 1 && gr.level[i][j] == gr.level[i][j+1]) || (i < gr.taille - 1 && gr.level[i][j] == gr.level[i+1][j])){ 
                        death_flag = 0;
                        goto Ongoing;
                    }
                }
            }

        
        }
    Ongoing:
        if (death_flag == 1)
        {
            affichePartie(gr);
            printf("PERDU\n");
            goto Liberate;
        }
    

        for(int i=0; i<gr.taille; i++)
        {
            for(int j=0; j<gr.taille; j++)
            { 
                if (gr.level[i][j] == 2048){      // Si le joueur a atteint 2048 alors il a gagné et le jeu s'arrête et on revient au menu
                    
                    affichePartie(gr);
                    printf("GAGNE\n");
                    printf("\n\n");
                    goto Liberate;
                }
            }
        }

    }



Liberate:
    free_tab2D(gr.level,gr.taille);
    msleep(1000);
    goto MainScreen;
Exit:   
    return 0;
}
