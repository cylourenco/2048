#include "misc.h"
#include "vector2D.h"

typedef struct grid{
    int taille;
    int ** level; 
    int nb_case_vide;   
    int score;
}grid;



grid initGrid();
void save(grid gr,char * filename);
void affichePartie(grid gr);
void pop(grid *, int);
int deplacement(grid * g, char c);
void reload(grid * g,char * filename);
