/*Définition des vecteurs 2D et fonctions utilitaires*/
#pragma once


typedef struct Vector2D{
    float x;
    float y;
}Vector2D;


Vector2D newVector2D(float x, float y);


Vector2D add(Vector2D v1, Vector2D v2);
Vector2D sub(Vector2D v1,Vector2D v2);

void set(Vector2D * pvector2D, float x , float y);

Vector2D scaledVersion(Vector2D vector2D, float scalar);
Vector2D map(Vector2D vector2D, float (*f)(float));
int diff(Vector2D v1, Vector2D v2);
int around(Vector2D v1,Vector2D v2, Vector2D v3);
int scalarProduct(Vector2D v1,Vector2D v2);