/*Fonctions utilitaires du programme*/

#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <errno.h>
#include "vector2D.h"




void free_tab2D(int** t, int n); 

void print_tab2D(int** t, int n);

int ** niveau(int taille);

void echange(int ** tab, int i1, int j1 , int i2 , int j2);

int msleep(long msec);

int nombre_nombre( int nb );