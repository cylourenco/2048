#pragma once
#include <SDL/SDL.h>
#include "vector2D.h"


typedef struct Texture
{
    SDL_Surface * grille;
    SDL_Surface * cases[11];
    SDL_Surface * menu;
}Texture;



typedef struct Bloc
{
    Vector2D vecteurPosition;
    Vector2D vitesse;
    Vector2D acc;
    Vector2D futurVecteurPosition;
    int valeur;
}Bloc;


extern Bloc gBloc[4][4];

void processEvents();
void draw();
void update();
