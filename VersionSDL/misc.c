#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include "misc.h"
#include "vector2D.h"
#include <math.h>




void free_tab2D(int** t, int n) {
    /*
        Libère le tableau 2D d'entiers t  de tqille n x n passé en paramètre .
    */
	if (t != NULL) {
		for (int i = 0; i < n; i++) {
			free(t[i]);
		};

		free(t);
	}
}

void print_tab2D(int** t, int n) {
    /*
        Affiche le tableau 2D t de taille n x n passé en paramètre dans des sortes de cases
    */
   if (n == 2){printf("-------------\n");}
   else if (n == 3){printf("-------------------\n");}
   else if (n == 4){printf("-------------------------\n");}
   else if (n == 5){printf("-------------------------------\n");}
   else if (n == 6){printf("-------------------------------------\n");}
   else if (n == 7){printf("-------------------------------------------\n");}
   else {printf("-------------------------------------------------\n");}
    
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			printf("|%4d ", t[i][j]);
		}
        if (n == 2){printf("|\n-------------\n");}
        else if (n == 3){printf("|\n-------------------\n");}
        else if (n == 4){printf("|\n-------------------------\n");}
        else if (n == 5){printf("|\n-------------------------------\n");}
        else if (n == 6){printf("|\n-------------------------------------\n");}
        else if (n == 7){printf("|\n-------------------------------------------\n");}
        else {printf("|\n-------------------------------------------------\n");}
		
	}
}

int ** niveau(int taille) {
    /*
        Génère un tableau  2D de la taille spécifiée
    */
    int ** t = malloc(taille*sizeof(int*));
    if (t != NULL) {
        for (int i = 0; i < taille; i++) { 
            t[i] = malloc(taille*sizeof(int));         
            for (int j = 0; j < taille; j++) {
                t[i][j] = 0;
            }
        }
    }
    return t;
}

void echange(int ** tab, int i1, int j1 , int i2 , int j2)
{
    /*
        Echange les éléments aux indices (i1,j1) et (j1,j2)
    */
    int temp = tab[i1][j1];
    tab[i1][j1] = tab[i2][j2];
    tab[i2][j2] = temp;
}

int msleep(long msec)
{
    /*
        Pause pour msec millisecondes
    */
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}



int nombre_nombre( int nb )
{

    /*Retourne le nombre de chiffres d'un nombre*/
    int i = 1;
    while (nb >= 10)
    {
        nb /= 10;
        i++;
    }
    return i;
}