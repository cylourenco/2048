#include <SDL/SDL_image.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include "constants.h"
#include "grid.h"
#include <math.h>
#include <SDL/SDL_ttf.h>
#include "misc.h"
#include "2048.h"

/*Retourne x ^ y*/
int integerPower(int x, int y)
{
    int result = 1;
    for (int i=1;i<=y;i++)  
    {
        result *= x;
    }
    return result;
}

int gContinuerPartie = 1;
SDL_Surface * gScreen;
SDL_Event gEvent;
Texture gTexture;
grid gGrille;
Bloc gBloc[4][4];
TTF_Font * gPolice;

int gMovementElapsed = 0;
SDL_Color blanc = {255,255,255};
SDL_Color noir = {0,0,0};
SDL_Color gris = {105,105,105};


int gZone = MENU;

int gMoving;


void processEvents()
{ 
    /* Gestion des évènements*/
    while (SDL_PollEvent(&gEvent))
    {
        switch (gEvent.type)
        {
            case SDL_QUIT:
                gContinuerPartie = 0;
                break;

            case SDL_USEREVENT:
                /* Animation de déplacement*/

                for (int i=0; i<4;i++) 
                {
                    for (int j=0; j<4;j++)
                    {
                        //D'abord , voir si le bloc est proche de sa position finale
                        Vector2D prochaineVitesse = add(gBloc[i][j].vitesse,gBloc[i][j].acc);
                        int close = around(gBloc[i][j].vecteurPosition,gBloc[i][j].futurVecteurPosition,prochaineVitesse);
                        if (close == 0 && scalarProduct(gBloc[i][j].vitesse,prochaineVitesse) >= 0) 
                        {
                            /*Si il ne l'est pas, accélérer vers la position finale*/
                            /*Revoir l'accélération*/

                            gBloc[i][j].vitesse = prochaineVitesse;
                            gBloc[i][j].vecteurPosition  = add(gBloc[i][j].vecteurPosition,gBloc[i][j].vitesse);
                        }
                        else 
                        {
                            /*Si oui, mettre le bloc à la position finale*/
                            set(&gBloc[i][j].vitesse,0,0);
                            set(&gBloc[i][j].acc,0,0);
                            gBloc[i][j].vecteurPosition = gBloc[i][j].futurVecteurPosition;
                        }
                    }
                }

                if (gMovementElapsed > MVTTIME)
                {
                    gMoving = 0;
                    gMovementElapsed = 0;
                }
                break;

            case SDL_KEYDOWN:
                
                switch (gEvent.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        gContinuerPartie = 0;
                        break;
                    /*Se déplacer uniquement si on ne se déplace pas déjà puis faire apparaître une 
                    nouvelle case si il y a eu un changement*/
                    case SDLK_LEFT:
                        if (!gMoving && (gMoving =  deplacement(&gGrille,'g')))
                        {
                            gMovementElapsed = 0;
                            pop(&gGrille,0);
                        }
                        

                        break;
                    case SDLK_RIGHT:
                        if (!gMoving && (gMoving = deplacement(&gGrille,'d')))
                        {   
                            gMovementElapsed = 0;
                            pop(&gGrille,0);
                        }
                        
                        break;
                    case SDLK_UP:
                        if (!gMoving && (gMoving = deplacement(&gGrille,'h')))
                        {
                            gMovementElapsed = 0;
                            pop(&gGrille,0);
                        }
                        
                        break;
                    case SDLK_DOWN:
                    
                        if (!gMoving && (gMoving = deplacement(&gGrille,'b')))
                        {
                            gMovementElapsed = 0;
                            pop(&gGrille,0);
                        }
                        
                        break;
                    default:
                        break;
                }
                case SDL_MOUSEBUTTONDOWN:
                
                    /*
                        Intéractivité souris-écran
                    */
                
                   /*Dans le menu*/
                    if (gZone == MENU)
                    {
                        if (gEvent.button.button == SDL_BUTTON_LEFT)
                        {
                            /*Nouvelle partie*/
                            if (gEvent.button.x > NOUVPARTIEXD && gEvent.button.x < NOUVPARTIEXF &&
                                gEvent.button.y > NOUVPARTIEYD && gEvent.button.y < NOUVPARTIEYF)
                            {
                                gZone = JEU;
                                free_tab2D(gGrille.level,gGrille.taille);
                                gGrille = initGrid();
                                gGrille.temps = 0;
                            }
                            /*Recharge de la partie sauvegardée*/
                            else if (gEvent.button.x > PARTIESAUVXD && gEvent.button.x < PARTIESAUVXF &&
                                gEvent.button.y > PARTIESAUVYD && gEvent.button.y < PARTIESAUVYF)
                            {
                                gZone = JEU;
                                reload(&gGrille,"sauvegarde.txt");
                            }
                            /*Quitter le jeu*/
                            else if (gEvent.button.x > QUITTERXD && gEvent.button.x < QUITTERXF &&
                                gEvent.button.y > QUITTERYD && gEvent.button.y < QUITTERYF)
                            {
                                gContinuerPartie = 0;
                            }
                            
                        }
                    }
                    /*Ecran de jeu principal*/
                    else if (gZone == JEU || gZone == GAMEWIN || gZone == GAMEOVER)
                    {
                        /*Bouton de retour vers le menu*/
                        if (gEvent.button.x > MAISONXD && gEvent.button.x < MAISONXF &&
                                gEvent.button.y > MAISONYD && gEvent.button.y < MAISONYF)
                        {
                            gZone = MENU;
                        }
                        /*Bouton de sauvegarde*/
                        else if (gEvent.button.x > SAUVXD && gEvent.button.x < SAUVXF &&
                                gEvent.button.y > SAUVYD && gEvent.button.y < SAUVYF)
                        {
                            save(gGrille,"sauvegarde.txt");
                        }
                        else if (gEvent.button.x > RESTARTXD && gEvent.button.x < RESTARTXF &&
                                gEvent.button.y > RESTARTYD && gEvent.button.y < RESTARTYF)
                        {
                            free_tab2D(gGrille.level,gGrille.taille);
                            gGrille = initGrid();
                            gMovementElapsed = 0;
                            gMoving = 0;
                            gZone = JEU;
                        }
                    }

                    break;
                
            default:
                break;
        }
    }
}



int main(int argc, char * argv[])
{


    const int FPS = 60;
    const int delay = 1000 / FPS;
    int frameStart, frameEnd,frameDelay;

    //Initialisation de la SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fprintf(stderr,"Erreur dans l'initialisation de la SDL : %s",SDL_GetError());
        return EXIT_FAILURE;
    }
    
    //Initialisation de l'icone 
    SDL_Surface * icon = IMG_Load("img/icon.bmp");


    if (icon == NULL)
    {
        fprintf(stderr,"Erreur dans l'initialisation de l'icone : %s",SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_WM_SetIcon(icon,NULL);
    SDL_FreeSurface(icon);


    //Initialisation de l'écran
    if ((gScreen = SDL_SetVideoMode(WIDTH,HEIGHT,32,SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL)
    {
      fprintf(stderr,"Erreur dans l'initialisation de l'écran : %s",SDL_GetError());
      SDL_Quit();
      return EXIT_FAILURE;

    }

    //Initialisation de True Type Font
    if(TTF_Init() == -1)
    {
        fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }


    //Titre de la fenêtre
    SDL_WM_SetCaption("2048 Par Cynthia Lourenço et Boris Ouya",NULL); 

    /*Initialisation des tuiles*/
    for (int i=0;i<11;i++)
    {
        char name[100];
        sprintf(name,"img/%d.bmp",integerPower(2,i+1));
        gTexture.cases[i] = SDL_LoadBMP(name);
    }

    /*Initialisation de l'image de jeu et de l'image du menu*/

    gTexture.grille = SDL_LoadBMP("img/jeu.bmp");
    gTexture.menu = SDL_LoadBMP("img/menu.bmp");
    

    
    //Ouverture de la police d'écriture
    gPolice = TTF_OpenFont("Fonts/arial.ttf",30);


    SDL_Rect pos;

    //Boucle principale du jeu
    while (gContinuerPartie)
    {
        

        frameStart = SDL_GetTicks();

        processEvents();

        //Peindre la fenêtre en blanc
        SDL_FillRect(gScreen,NULL,SDL_MapRGB(gScreen->format,255,255,255));

        
            
        //Afficher les blocs si on est dans l'écran de jeu principal ou l'écran de défaite
        if (gZone == JEU || gZone == GAMEOVER || gZone == GAMEWIN)
        {
            pos.x = pos.y = 0;
            pos.w = 480;
            pos.h = 640;
            //Mise en place de l'image de la grille
            SDL_BlitSurface(gTexture.grille,NULL,gScreen,&pos);

            //Si il n'y a pas de mouvement, mettre à jour le tableau des blocs
            if (!gMoving)
            {
                for (int i=0;i<4;i++)
                {
                    for (int j=0;j<4;j++)
                    {
                        Vector2D v = {j,i};
                        gBloc[i][j].vecteurPosition = gBloc[i][j].futurVecteurPosition = v;
                        gBloc[i][j].valeur = gGrille.level[i][j];   
                        gBloc[i][j].vitesse = newVector2D(0,0);
                    }
                }   
            }
            /*Sinon, on bouge. On envoie donc un évènement de mouvement à la SDL*/
            else
            {
                SDL_Event move;
                move.type = SDL_USEREVENT;
                SDL_PushEvent(&move);
            }


            /*Mettre les placeholders*/
            for (int i=0;i<4;i++)
            {
                for (int j=0;j<4;j++)
                {
                    pos.x = GRILLEXDEBUT + j * INBETWEENXSPACE;
                    pos.y = GRILLEYDEBUT + i * INBETWEENYSPACE;
                    SDL_Surface * rect = SDL_CreateRGBSurface(SDL_HWSURFACE,BLOCWIDTH,BLOCHEIGHT,32,0,0,0,0);
                    SDL_FillRect(rect,NULL,SDL_MapRGB(rect->format,255,255,255));
                    SDL_BlitSurface(rect,NULL,gScreen,&pos);
                    SDL_FreeSurface(rect);
                }
            }

            //Afficher les blocs à la position désignée par leur vecteur position
            for (int i=0;i<4;i++)
            {
                for (int j=0;j<4;j++)
                {
                    if (gBloc[i][j].valeur != 0)
                    {
                        int index = log2(gBloc[i][j].valeur) - 1;
                        pos.x = GRILLEXDEBUT + gBloc[i][j].vecteurPosition.x * INBETWEENXSPACE;
        
                        pos.y = GRILLEYDEBUT + gBloc[i][j].vecteurPosition.y * INBETWEENYSPACE;
                        SDL_BlitSurface(gTexture.cases[index],NULL,gScreen,&pos);
                    }
                    
                }
            }

        }

        //Dans le menu
        else if (gZone == MENU)
        {
            //Affichage de l'image du menu
            pos.x = pos.y = 0;
            pos.w = 480;
            pos.h = 640;
            SDL_BlitSurface(gTexture.menu,NULL,gScreen,&pos);
        }
        //Ecran de défaite
        if (gZone == GAMEOVER || gZone == GAMEWIN)
        {
            SDL_Rect pos;
            pos.x = GRILLESTARTXD;
            pos.y = GRILLESTARTYD;

            //Ecran  transparent
            SDL_Surface * over = SDL_CreateRGBSurface(SDL_HWSURFACE,WIDTHGRILLE,HEIGHTGRILLE,32,0,0,0,0);
            SDL_FillRect(over,NULL,SDL_MapRGB(over->format,gris.r,gris.g,gris.b));
            SDL_SetAlpha(over, SDL_SRCALPHA, 100);
            SDL_BlitSurface(over,NULL,gScreen,&pos);
            SDL_FreeSurface(over);

            //Affichage du temps
            char label_temps[10000];
            int mns = (gGrille.temps / 1000 ) / 60;
            int seconds = gGrille.temps / 1000 - mns * 60;
            sprintf(label_temps,"%2d mn :%2ds",mns,seconds);
            SDL_Surface * time = TTF_RenderText_Solid(gPolice,label_temps,blanc);
            SDL_Rect posTime;
            posTime.x = TIMEXD;
            posTime.y = TIMEYD;
            SDL_BlitSurface(time,NULL,gScreen,&posTime);
            SDL_FreeSurface(time);

            char label_state[10000];
    

            if (gZone == GAMEOVER)
            {
                sprintf(label_state,"PERDU");
            }
            else if (gZone == GAMEWIN)
            {
                sprintf(label_state,"GAGNE");
            }
            
            
            TTF_Font * font = TTF_OpenFont("Fonts/arial.ttf",45);
            SDL_Surface * lose = TTF_RenderText_Solid(font,label_state,blanc);
            SDL_Rect poslose;
            poslose.x = LABELFINXD;
            poslose.y = LABELFINYD;
            SDL_BlitSurface(lose,NULL,gScreen,&poslose);

            TTF_CloseFont(font);

            //Free font
            SDL_FreeSurface(lose);

            
        }
        
        if (gZone == GAMEOVER || gZone == JEU || gZone == GAMEWIN)
        {
            /*Affichage du score*/
            char label_score[100000];

            int nb = nombre_nombre(gGrille.score);

            sprintf(label_score,"%d",gGrille.score);
            SDL_Rect posScore;
            posScore.x = SCOREXD - (nb - 1)* 10;
            posScore.y = SCOREYD;
            SDL_Surface * score = TTF_RenderText_Solid(gPolice,label_score,blanc);
            SDL_BlitSurface(score,NULL,gScreen,&posScore);
            SDL_FreeSurface(score);
        }

        frameEnd = SDL_GetTicks();

        frameDelay = frameEnd - frameStart;

       
        //Délai
        if (frameDelay < delay)
        {
            SDL_Delay(delay - frameDelay);
        }
        
        //Affichage du temps  //MERGE AVEC CELUI DANS GAMEOVER
        if (gZone == JEU)
        {
            gGrille.temps += delay;
            char label_temps[10000];
            int mns = (gGrille.temps / 1000 ) / 60;
            int seconds = gGrille.temps / 1000 - mns * 60;
            sprintf(label_temps,"%2d mn :%2ds",mns,seconds);
            SDL_Surface * time = TTF_RenderText_Solid(gPolice,label_temps,blanc);
            SDL_Rect posTime;
            posTime.x = TIMEXD;
            posTime.y = TIMEYD;
            SDL_BlitSurface(time,NULL,gScreen,&posTime);
            SDL_FreeSurface(time);
        }

        
        SDL_Flip(gScreen);
        
        //Peut-on esperer continuer la partie en cours?

        //Charger les écrans de fin de jeu si nécessaire
        if (gZone == JEU && !peutBouger(gGrille))
        {
            gZone = GAMEOVER;
        }
    
        if (gZone == JEU && victoire(gGrille))
        {
            gZone = GAMEWIN;
        }

        //Mise à jour du timer de jeu

        if (gMoving)
        {
            gMovementElapsed += delay;

        }


    }

    

    for (int i=0;i<11;i++)
    {
        SDL_FreeSurface(gTexture.cases[i]);
    }


    SDL_FreeSurface(gTexture.grille);
    SDL_FreeSurface(gTexture.menu);
    free_tab2D(gGrille.level,gGrille.taille);
    SDL_FreeSurface(gScreen);
    IMG_Quit();
    TTF_CloseFont(gPolice);
    TTF_Quit();
    SDL_Quit();
    

    return EXIT_SUCCESS;
}