#include "grid.h"
#include "2048.h"




void pop(grid * g, int a)
{
    /*
        si a = 1 c'est à dire au début de la partie:
            Met un 2 sur une case aléatoire du niveau de grid
        si a = 0 c'est à dire au cours de la partie:
            Met un 2 ou un 4 (uniformément choisi) sur une case aléatoire du niveau de grid
    */
    srand(time(NULL));
    
    int nb=0;
    if (g->nb_case_vide != 0)
    {
        
        int b;
        if (a == 1){
            b = 2;
        }
        else{
            b = (rand() % 2 == 1) ? 2 : 4;
        }

        int i,j;
        i = rand() % g->taille;
        j =  rand() % g->taille;
        while (g->level[i][j] != 0)
        {
            i = rand() % g->taille;
            j =  rand() % g->taille;
        } 

        g->level[i][j] = b;

        g->nb_case_vide -= 1;  
    }
    
}

int deplacement(grid * g, char c)
{  
    //Déplacement /fusion logique des blocs sur la grille
    

    /*
        Retourne 1 si un changement a eu lieu (déplacement ou fusion) et 0 sinon
    */

    int change = 0;
    int fusion_flag = 0; //marqueur de fusion

    Vector2D vec = {0,0}; // vecteur déplacement
    Vector2D dir; //vecteur directionnel

    if (c == 'g')
    {
        set(&dir,-1,0);

        for (int i = 0; i < g->taille; i += 1)
        {
            //Réinitialiser le vecteur de déplacement et le marqueur de fusion
            set(&vec,0,0);
            fusion_flag = 0;

            for (int j=0; j < g->taille; j += -dir.x)
            {
                //Si on rencontre un 0, "incrémenter" le vecteur et préciser qu'aucune fusion n'a été faite
                if (g->level[i][j] == 0)
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else
                {

                    /*S'il doit y avoir un déplacement, préciser qu'un changement a eu lieu*/
                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  

                    Vector2D pos = {j,i};
                    Vector2D newpos = add(pos,vec);
                    Vector2D checkpos = add(newpos,dir);

                    /*Si la case suivant celle où devrait se finir le déplacement à la même valeur que la 
                    case déplacée, mettre cette case au double et la case qui devrait être déplacée à 0 si aucune fusion
                    ne s'est déroulée à l'itération précédente. 
                    Sinon, déplacer la case à l'emplacement prévu et préciser qu'il n'y a eu aucune fusion
                    */
                    


                    float vitesse_initiale;
                    
                    
                    if (newpos.x > 0 && g->level[(int)checkpos.y][(int)checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                        g->level[(int)checkpos.y][(int)checkpos.x] *= 2;
                        g->level[i][j] = 0; 
                        g->score += g->level[(int)checkpos.y][(int)checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;  // changement (fusion)
                        fusion_flag = 1;

                        gBloc[i][j].futurVecteurPosition = checkpos;
                    } 
                    else 
                    {
                        gBloc[i][j].futurVecteurPosition = newpos;
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);
                    }  

                    
                    //Initialisation de la vitesse et de l'accélération du bloc
                    vitesse_initiale = sqrt(2*(gBloc[i][j].vecteurPosition.x - gBloc[i][j].futurVecteurPosition.x)/MVTTIME);
                    gBloc[i][j].acc = scaledVersion(dir,-vitesse_initiale / MVTTIME);
                    gBloc[i][j].vitesse = scaledVersion(dir,vitesse_initiale); 
                    
                } 

            } 
        }   
    }
    else if (c == 'd')
    {
       set(&dir,1,0);

       for (int i = 0; i < g->taille; i += 1)
        {
            fusion_flag = 0;
            set(&vec,0,0);

            for (int j = g->taille - 1; j >= 0; j += -dir.x)
            {
                if (g->level[i][j] == 0)
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else
                {

                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  
                    Vector2D pos = {j,i};
                    Vector2D newpos = add(pos,vec);
                    Vector2D checkpos = add(newpos,dir);


                    float vitesse_initiale;              

                    if (newpos.x < g->taille - 1 && g->level[(int)checkpos.y][(int)checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                          
                        g->level[(int)checkpos.y][(int)checkpos.x] *= 2;
                        g->level[i][j] = 0; 
                        g->score += g->level[(int)checkpos.y][(int)checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;
                        fusion_flag = 1;

                        gBloc[i][j].futurVecteurPosition = checkpos;
                        vitesse_initiale =  sqrt(2*(-gBloc[i][j].vecteurPosition.x + checkpos.x)/MVTTIME);
                    } 
                    else
                    {
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);
                        gBloc[i][j].futurVecteurPosition = newpos;
                    }  

                    vitesse_initiale = sqrt(2*(-gBloc[i][j].vecteurPosition.x + gBloc[i][j].futurVecteurPosition.x)/MVTTIME);
                    gBloc[i][j].acc = scaledVersion(dir,-vitesse_initiale / MVTTIME);
                    gBloc[i][j].vitesse = scaledVersion(dir,vitesse_initiale);

                    
                } 

            }
        }   
    }  
    else if (c == 'b')
    {
       set(&dir,0,1);

       for (int j = 0; j < g->taille; j += 1)
        {
            set(&vec,0,0);
            fusion_flag = 0;

            for (int i=g->taille - 1; i >= 0; i += -dir.y)
            {
                if (g->level[i][j] ==  0)
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else
                {
                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  
                    Vector2D pos = {j,i};
                    Vector2D newpos = add(pos,vec);
                    Vector2D checkpos = add(newpos,dir);

                    
                    float vitesse_initiale;
                    

                    if (newpos.y < g->taille - 1 && g->level[(int)checkpos.y][(int)checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                        //fusion
                        
                        g->level[(int)checkpos.y][(int)checkpos.x] *= 2;
                        g->level[i][j] = 0; 
                        g->score += g->level[(int)checkpos.y][(int)checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;
                        fusion_flag = 1;

                        gBloc[i][j].futurVecteurPosition = checkpos;
                    
                    } 
                    else 
                    {
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);

                        gBloc[i][j].futurVecteurPosition = newpos;
                    }

                    vitesse_initiale = sqrt(2*(-gBloc[i][j].vecteurPosition.y + gBloc[i][j].futurVecteurPosition.y)/MVTTIME);
                    gBloc[i][j].acc = scaledVersion(dir,-vitesse_initiale / MVTTIME);
                    gBloc[i][j].vitesse = scaledVersion(dir,vitesse_initiale);              
                } 

            }
        }
    }
    else if (c == 'h')
    {
        set(&dir,0,-1);

        for (int j = 0; j < g->taille; j += 1)
        {
            set(&vec,0,0);
            fusion_flag = 0;

            for (int i=0; i < g->taille; i += -dir.y)
            {
                if (g->level[i][j] ==   0)
                {    
                    vec = add(vec,dir);
                    fusion_flag = 0;
                } 
                else
                {

                    if (vec.x != 0 || vec.y != 0)
                    {
                        change = 1;
                    }  
                    Vector2D pos = {j,i};
                    Vector2D newpos = add(pos,vec);
                    Vector2D checkpos = add(newpos,dir);

                    float vitesse_initiale;

                    if (newpos.y > 0 && g->level[(int)checkpos.y][(int)checkpos.x] == g->level[i][j] && fusion_flag == 0)
                    {
                        //fusion
                        
                        g->level[(int)checkpos.y][(int)checkpos.x] *= 2;
                        g->level[i][j] = 0; 
                        g->score += g->level[(int)checkpos.y][(int)checkpos.x];
                        vec = add(vec,dir);
                        g->nb_case_vide += 1;
                        change = 1;
                        fusion_flag = 1;
                        gBloc[i][j].futurVecteurPosition = checkpos;
                    } 
                    else 
                    {
                        fusion_flag = 0;
                        echange(g->level,pos.y,pos.x,newpos.y,newpos.x);

                        gBloc[i][j].futurVecteurPosition = newpos;
                    }  

                    vitesse_initiale = sqrt(2*(gBloc[i][j].vecteurPosition.y - gBloc[i][j].futurVecteurPosition.y)/MVTTIME);
                    gBloc[i][j].acc = scaledVersion(dir,-vitesse_initiale / MVTTIME);
                    gBloc[i][j].vitesse = scaledVersion(dir,vitesse_initiale);

                } 

            }
        }
    }  

    
    return change;

}


void reload(grid * g,char * filename)
{

    /*
        Chargement d'une grille sauvegardée dans filename
    */
    FILE * fichier = NULL;

    if ((fichier = fopen(filename,"r+")) != NULL)
    {
        fscanf(fichier,"%d\n",&g->taille);
        g->level = niveau(g->taille);

        for (int i=0; i < g->taille * g->taille;i++)
        {
            if (i % g->taille != g->taille - 1)
            {
                fscanf(fichier,"%d ",&g->level[i/g->taille][i%g->taille]);
            }
            else
            {
                fscanf(fichier,"%d\n",&g->level[i/g->taille][i%g->taille]);
            }
        }        

        fscanf(fichier,"%d\n",&g->score);
        fscanf(fichier,"%d\n",&g->nb_case_vide);
        fscanf(fichier,"%d\n",&g->temps);

    }


}

grid initGrid()
{
    /*
        Initialisation de la grille
    */
    grid gr;
    gr.taille = 4;
    gr.level = niveau(gr.taille);
    gr.nb_case_vide = gr.taille * gr.taille ;
    gr.temps = 0;

    pop(&gr,1);
    pop(&gr,1);
    gr.score = 0;

    return gr;
}

void save(grid gr,char * filename)
{
    /*
        Sauvegarde de la partie actuelle dans le fichier filename
    */
    FILE *fichier;
 
    fichier = fopen (filename, "w+");
    
    if (fichier != NULL)
    {
        fprintf(fichier,"%d\n",gr.taille);
        for(int i=0; i<gr.taille ; i++)
        {
            for(int j=0; j<gr.taille ; j++)
            { 
                fprintf (fichier, "%d ", gr.level[i][j]);
            }
            fprintf (fichier, "\n");
        }
        fprintf(fichier, "%d\n", gr.score);
        fprintf(fichier, "%d\n",gr.nb_case_vide);
        fprintf(fichier, "%d\n",gr.temps);
        fclose (fichier);
    }

}

void affichePartie(grid gr)
{
    /*
        Affichage de l'état de la partie en cours
    */
    print_tab2D(gr.level,gr.taille);
    printf("Score: %d\n", gr.score);
    msleep(500);
}

int peutBouger(grid gr)
{
    //Retourne 1 si un mouvement est possible, 0 sinon
    if (gr.nb_case_vide == 0){    // Si il n'y a plus de case de case vide et si il y a plus aucune combinaison possible alors le joueur a perdu
        
            for(int i=0; i<gr.taille ; i++)
            {
                for(int j=0; j<gr.taille ; j++)
                { 
                    if ((j < gr.taille - 1 && gr.level[i][j] == gr.level[i][j+1]) || (i < gr.taille - 1 && gr.level[i][j] == gr.level[i+1][j])){ 
                        return 1;
                    }
                    else if (i== gr.taille - 1 && j != gr.taille -1 && gr.level[i][j] == gr.level[i][j+1])
                    {
                        return 1;
                    }
                    else if (j== gr.taille - 1 && i != gr.taille -1 && gr.level[i][j] == gr.level[i+1][j])
                    {
                        return 1;
                    }
                }
            }

    }
    else
    {
        return 1;
    }
    return 0;
}

int victoire(grid g)
{
    //Retourne 1 s'il existe une case 2048, 0 sinon
    for(int i=0; i<g.taille; i++)
    {
        for(int j=0; j<g.taille; j++)
        { 
            if (g.level[i][j] == 2048){      // Si le joueur a atteint 2048 alors il a gagné
                return 1;
            }
        }
    }

    return 0;
}


