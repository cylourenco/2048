# 2048

Le jeu 2048 en C soit en version console ou avec SDL.

## Démarrage

Pour compiler ce programme, mettez vous dans le dossier que vous voulez et écrivez :
$make
$./2048

Il faut avoir installer SDL pour pouvoir compiler la version graphique de 2048.

## Auteurs
* **Cynthia LOURENCO** _alias_ [@cylourenco](https://gitlab.isima.fr/users/cylourenco)
* **Boris OUYA** _alias_ [@boouya](https://gitlab.isima.fr/users/boouya)



